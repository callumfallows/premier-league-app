const Fonts = {
    heading: "Inter",
    body: "Inter",
    span: "Inter",
    button: "Inter",
    input: "Inter",
    textarea: "Inter",
    a: "Inter",
    p: "Inter"
};

export { Fonts };
