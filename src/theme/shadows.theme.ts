const Shadows = {
    lg: "0px 10px 15px -3px rgba(23, 25, 35, 0.1), 0px 4px 6px -2px rgba(23, 25, 35, 0.05)",
    filterModal: "0px 20px 25px -5px rgba(23, 25, 35, 0.1), 0px 10px 10px -5px rgba(23, 25, 35, 0.04)"
};

export { Shadows };
