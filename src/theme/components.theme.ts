const Components = {
    Text: {
        baseStyle: {
            fontFamily: "Inter",
            fontSize: 14
        },
    },
    Heading: {
        baseStyle: {
            fontFamily: "Inter",
            fontSize: 24
        },
    }
};

export { Components };
