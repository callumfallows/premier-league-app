import { extendTheme } from "@chakra-ui/react";

import { Colours } from "./theme/colours.theme";
import { Fonts } from "./theme/fonts.theme";
import { Global } from "./theme/global.theme";
import { Components } from "./theme/components.theme";

import "@fontsource/inter/400.css";
import "@fontsource/inter/500.css";
import "@fontsource/inter/600.css";
import "@fontsource/inter/700.css";

const theme = extendTheme({
    colors: Colours,
    components: Components,
    fonts: Fonts,
    global: Global
});

export { theme };
