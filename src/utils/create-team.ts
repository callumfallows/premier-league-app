function ValidateTeam(team: any, player: any, position: any):
    {
        message: string,
        isValid: boolean
    } {

    const GoalKeeper = team.filter((teamPlayer: any) => teamPlayer.position === "goal_keeper");
    const Defenders = team.filter((teamPlayer: any) => teamPlayer.position === "defender");
    const Midfielders = team.filter((teamPlayer: any) => teamPlayer.position === "midfielder");
    const Strikers = team.filter((teamPlayer: any) => teamPlayer.position === "striker");
    const ExistingPlayer = team.filter((teamPlayer: any) => teamPlayer.player.code === player.code);

    //rules for a valid team are as follows:
    //An 11-player team must only contain 1 Goal-keeper, 2-5 Defenders, 1-4 Midfielders, 1-3 Strikers.
    let _message = "Valid"
    let _isValid = true
    if (GoalKeeper.length > 0 && position === "goal_keeper") {
        _message = "Too many goalkeepers, only 1 can be selected"
        _isValid = false
    }
    if (Defenders.length > 4 && position === "defender") {
        _message = "Too many defenders, only 2 - 5 can be selected"
        _isValid = false
    }
    if (Midfielders.length > 3 && position === "midfielder") {
        _message = "Too many midfielders, only 1 - 4 can be selected"
        _isValid = false
    }
    if (Strikers.length > 2 && position === "striker") {
        _message = "Too many strikers, only 1 - 4 can be selected"
        _isValid = false
    }

    if (team.length > 10) {
        _message = "Too many players, only a max of 11 can be selected"
        _isValid = false
    }

    if (ExistingPlayer.length > 0) {
        _message = "Player has already been selected"
        _isValid = false
    }

    // console.log('ExistingPlayer', ExistingPlayer);
    // console.log('GoalKeeper', GoalKeeper);
    // console.log('Defenders', Defenders);
    // console.log('Midfielders', Midfielders);
    // console.log('Strikers', Strikers);
    // console.log('Team', team.length);

    return {
        message: _message,
        isValid: _isValid
    };
}

export {ValidateTeam}