import { history } from "../routes/history";

function deleteURLParam(name: string): void {
    const url = new URL(window.location.href);
    url.searchParams.delete(name);

    history.push({
        pathname: url.pathname,
        search: url.search
    });
}

function setURLParam(name: string, value: string, skipAddToBrowserHistory?: boolean): void {
    const url = new URL(window.location.href);
    url.searchParams.set(name, value);

    /**
     * If we are setting default perams,
     * no need to change history here
     */
    if (skipAddToBrowserHistory) {
        history.replace({
            pathname: url.pathname,
            search: url.search
        });
        return;
    }

    history.push({
        pathname: url.pathname,
        search: url.search
    });
}

export {
    deleteURLParam,
    setURLParam
};
