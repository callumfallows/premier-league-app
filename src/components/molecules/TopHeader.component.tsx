import * as React from "react";
import {Box, Flex, Heading, Text} from "@chakra-ui/react";

function TopHeaderComponent(): JSX.Element {

    return <Flex
        width="100%"
        padding="4"
        alignItems="center"
        justifyContent="space-between"
    >
        <Box>
            <Heading color="grey.600" size="xl">
                Fantasy Premier League Manager
            </Heading>
        </Box>
        <Box>
            <Text>Placeholder</Text>
        </Box>
    </Flex>
}

export { TopHeaderComponent }