import * as React from "react";
import {
    Input,
    VStack,
    Box,
    Text,
    FormLabel,
    InputGroup,
    FormControl,
    HStack,
    IconButton,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverArrow,
    Select,
    Button,
    PopoverCloseButton,
    PopoverBody,
    Portal,
    Flex,
} from "@chakra-ui/react";
import {AddIcon, CloseIcon} from "@chakra-ui/icons";

type PlayerSearchComponentProps = {
    playerName: string
    visible: boolean
    filteredPlayers: Array<any>
    onChange: any
    onClick: any
    onClose: any
    onBlur: any
    onFocus: any
}

function PlayerSearchComponent(
    {
        playerName,
        filteredPlayers,
        onChange,
        visible,
        onBlur,
        onFocus,
        onClick,
        onClose
    }: PlayerSearchComponentProps): JSX.Element {

    const [position, setPosition] = React.useState("goal_keeper")

    return <VStack space={4} width="100%" position="relative">
        <InputGroup>
            <FormControl>
                <FormLabel
                    htmlFor='playerSearch'
                >
                    Players
                </FormLabel>
                <Input
                    id="playerSearch"
                    type="text"
                    value={playerName}
                    onChange={onChange}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    placeholder='Search for players'
                />
            </FormControl>
        </InputGroup>
        {visible && filteredPlayers && filteredPlayers.length > 0 && <Box
            position="absolute"
            zIndex="4"
            backgroundColor="white"
            top="70px"
            maxHeight="500px"
            overflowX="scroll"
            borderRadius={2}
            border="1px solid"
            borderColor="gray.300"
            borderBottom="0"
            width="100%">
            <HStack
                padding={2}
                width="100%"
                alignItems="flex-end"
            >
                <Flex
                    alignItems="center"
                    justifyContent="space-between"
                    paddingBottom="8px"
                    borderBottom="1px solid"
                    borderColor="gray.200"
                    width="100%"
                >
                    <Text
                        fontSize="16px"
                        fontWeight="500"
                        color="gray.700"
                    >
                        Select Player
                    </Text>
                    <CloseIcon
                        weight="12px"
                        height="12px"
                        color='gray.500'
                        cursor="pointer"
                        onClick={onClose}
                    />
                </Flex>
            </HStack>
            {filteredPlayers.map((player: any, index: number) =>
                <Box
                    key={index}
                    _hover={{backgroundColor: "gray.200"}}
                    cursor="pointer"
                    backgroundColor="white"
                    borderBottom="1px"
                    borderColor="gray.300"
                    width="100%"
                    alignItems="baseline"
                >
                    <Box display="inline-flex">
                        <Popover placement="right">
                            <PopoverTrigger>
                                <HStack padding={2}>
                                    <IconButton
                                        size="sm"
                                        aria-label='Add player'
                                        icon={<AddIcon/>}
                                    />
                                    <Text>
                                        {`${player.first_name} ${player.second_name}`}
                                    </Text>
                                </HStack>
                            </PopoverTrigger>
                            <Portal>
                                <PopoverContent>
                                    <PopoverArrow/>
                                    <PopoverCloseButton/>
                                    <PopoverBody>
                                        <VStack spacing={2}>
                                            <Text fontWeight="500">
                                                {`${player.first_name} ${player.second_name}`}
                                            </Text>
                                            <Select onChange={(event) => {
                                                setPosition(event.target.value);
                                            }}
                                                    placeholder='Choose position'>
                                                <option
                                                    value='goal_keeper'
                                                >
                                                    Goal Keeper
                                                </option>
                                                <option
                                                    value='defender'
                                                >
                                                    Defender
                                                </option>
                                                <option
                                                    value='midfielder'
                                                >
                                                    Midfielder
                                                </option>
                                                <option
                                                    value='striker'
                                                >
                                                    Striker
                                                </option>
                                            </Select>
                                        </VStack>
                                    </PopoverBody>
                                    <Button
                                        margin={2}
                                        colorScheme="primary"
                                        disabled={!position}
                                        onClick={() => {
                                            onClick(player, position);
                                        }}>
                                        Add Player
                                    </Button>
                                </PopoverContent>
                            </Portal>
                        </Popover>
                    </Box>
                </Box>)
            }
        </Box>}
    </VStack>
}

export {PlayerSearchComponent}