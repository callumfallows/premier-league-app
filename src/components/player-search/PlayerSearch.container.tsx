import * as React from "react";
import {PlayerSearchComponent} from "./PlayerSearch.component";
import {debounce} from "lodash";
import {setURLParam} from "../../utils/router";

const debouncedSetURLParam = debounce(
    setURLParam,
    0
);

type PlayerSearchContainerProps = {
    playerName: string
    players?: Array<any>
    onSelectPlayer: (player: any, position: any) => any;
}

const FilterPlayers = (players: any, term: string) => {

    if (players && players.length > 0) {
        return players.filter((player: any) =>
            player.web_name.toLowerCase() === term.toLowerCase() ||
            player.first_name.toLowerCase() === term.toLowerCase() ||
            player.second_name.toLowerCase() === term.toLowerCase()
        )
    }
    return []
}

function PlayerSearchContainer(
    {
        playerName,
        players,
        onSelectPlayer
    }: PlayerSearchContainerProps): JSX.Element {

    const [term, setTerm] = React.useState("")
    const [visible, setVisible] = React.useState(false)
    const [filteredPlayers, setFilteredPlayers]: Array<any> = React.useState([])

    //hook for setting default
    React.useEffect(() => {
        setTerm(playerName)
    }, []);

    //hook for setting filtered
    React.useEffect(() => {
        let _filteredPlayers: Array<any> = FilterPlayers(players, term);
        setFilteredPlayers(_filteredPlayers)
    }, [players]);

    //hook for filtering
    React.useEffect(() => {
        let _filteredPlayers: Array<any> = FilterPlayers(players, term);
        setFilteredPlayers(_filteredPlayers)
    }, [term]);

    return <PlayerSearchComponent
        playerName={term}
        visible={visible}
        onClick={onSelectPlayer}
        onChange={({target}: any) => {
            debouncedSetURLParam("playerName", target.value, false)
            setTerm(target.value)
        }}
        filteredPlayers={filteredPlayers}
        onFocus={(event: any) => {
             setVisible(true)
        }}
        onClose={(event: any) => {
            setVisible(false)
        }}
        onBlur={(event: any) => {
            // setTimeout(() => setVisible(false), 200)
        }}
    />
}

export {PlayerSearchContainer}