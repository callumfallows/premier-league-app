import * as React from "react";
import {TeamSearchComponent} from "./TeamSearch.component";
import {debounce} from "lodash";
import {deleteURLParam, setURLParam} from "../../utils/router";

const debouncedSetURLParam = debounce(
    setURLParam,
    0
);

type TeamSearchContainerProps = {
    teamName: string
    teams?: Array<any>
    onSelectTeam?: (team: any) => any;
}


const FilterTeams = (teams: any, term: string) => {

    if (teams && teams.length > 0) {
        return teams.filter((team: any) =>
            team.name.toLowerCase() === term.toLowerCase() ||
            team.short_name.toLowerCase() === term.toLowerCase()
        )
    }
    return []
}

function TeamSearchContainer(
    {
        teamName,
        teams,
        onSelectTeam
    }: TeamSearchContainerProps): JSX.Element {

    const [term, setTerm] = React.useState("")
    const [visible, setVisible] = React.useState(false)
    const [filteredTeams, setFilteredTeams]: Array<any> = React.useState([])

    //hook for setting default
    React.useEffect(() => {
        setTerm(teamName)
    }, []);

    //hook for setting filtered
    React.useEffect(() => {
        let _filteredTeams: Array<any> = FilterTeams(teams, term)
        setFilteredTeams(_filteredTeams)
    }, [teams]);


    //hook for filtering
    React.useEffect(() => {
        let _filteredTeams: Array<any> = FilterTeams(teams, term)
        setFilteredTeams(_filteredTeams)
    }, [term]);

    return <TeamSearchComponent
        onChange={({target}: any) => {
            if (target.value.length === 0) {
                deleteURLParam("teamName");
            } else {
                setTerm(target.value)
                debouncedSetURLParam("teamName", target.value, false)
            }
        }}
        visible={visible}
        filteredTeams={filteredTeams}
        onClick={onSelectTeam}
        onFocus={(event: any) => {
            setVisible(true)
        }}
        onBlur={(event: any) => {
            setTimeout(() => setVisible(false), 200)
        }}
        teamName={term}
    />
}

export {TeamSearchContainer}