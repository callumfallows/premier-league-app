import * as React from "react";
import {
    Input,
    VStack,
    HStack,
    Box,
    Text,
    InputGroup,
    FormControl,
    FormLabel
} from "@chakra-ui/react";

type TeamSearchComponentProps = {
    teamName: string
    visible: boolean
    filteredTeams: Array<any>
    onChange: any
    onClick: any
    onBlur: any
    onFocus: any
}

function TeamSearchComponent(
    {
        teamName,
        visible,
        filteredTeams,
        onChange,
        onBlur,
        onClick,
        onFocus
    }: TeamSearchComponentProps): JSX.Element {

    return <VStack space={4} width="100%">
        <InputGroup width="100%">
            <FormControl width="100%">
                <FormLabel
                    htmlFor='teamSearch'
                    width="100%"
                >
                    Teams
                </FormLabel>
                <Input
                    width="100%"
                    id="teamSearch"
                    type="text"
                    value={teamName}
                    onChange={onChange}
                    onBlur={onBlur}
                    onFocus={onFocus}
                    placeholder='Search for teams'
                />
            </FormControl>
        </InputGroup>
        {visible && filteredTeams && filteredTeams.length > 0 &&
            <Box
                borderRadius={2}
                border="1px solid"
                borderColor="gray.300"
                borderBottom="0"
                width="100%">
                {filteredTeams.map((team: any, index: number) =>
                    <VStack
                        key={index}
                        onClick={() => {
                            onClick(team);
                        }}
                        cursor="pointer"
                        borderBottom="1px"
                        borderColor="gray.300"
                        width="100%"
                        alignItems="baseline"
                    >
                        <HStack padding={2} spacing={1}>
                            <Text fontWeight="500">
                                Team:
                            </Text>
                            <Text>
                                {`${team.name}`}
                            </Text>
                            <Text
                                textTransform="uppercase"
                                fontWeight="500"
                            >
                                ({`${team.short_name}`})
                            </Text>
                        </HStack>
                    </VStack>)
                }
            </Box>}
    </VStack>
}

export {TeamSearchComponent}