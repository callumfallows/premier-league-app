import * as React from "react";
import { Box } from "@chakra-ui/react";

type ContainerComponentProps = {
    children: JSX.Element | Array<JSX.Element>
}

function ContainerComponent(
    {
        children
    }: ContainerComponentProps): JSX.Element {

    return <Box
        maxWidth="1200px"
        margin="0 auto"
    >
        {children}
    </Box>
}

export {ContainerComponent}