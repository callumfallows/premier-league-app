import * as React from "react";
import {Text, Heading, VStack, HStack, IconButton} from "@chakra-ui/react";
import {MinusIcon} from "@chakra-ui/icons";

type MyTeamComponentProps = {
    myTeam: Array<any>
    onRemovePlayer: (player: any) => void
}

enum PlayerPosition {
    "goal_keeper" = "Goal Keeper",
    "midfielder" = "Midfielder",
    "defender" = "Defender",
    "striker" = "Striker"
}

function MyTeamComponent(
    {
        myTeam,
        onRemovePlayer
    }: MyTeamComponentProps) {

    return <VStack
        alignItems="flex-start"
        spacing="8"
        width="100%"
    >
        <Heading textAlign="left" size="lg">
            My Team
        </Heading>
        <VStack alignItems="flex-start">
            {myTeam?.map((
                {
                    player,
                    position
                }: {
                    player: any,
                    position: "goal_keeper" | "defender" | "striker" | "midfielder"
                }, index: number) =>
                <HStack spacing={4} key={index}>
                    <Text>
                        {`${player.first_name} ${player.second_name}`}
                    </Text>
                    <Text>
                        {PlayerPosition[position]}
                    </Text>
                    <IconButton
                        size="sm"
                        aria-label='Add player'
                        icon={<MinusIcon/>}
                        onClick={() => {
                            onRemovePlayer(player);
                        }}
                    />
                </HStack>)}
        </VStack>
    </VStack>
}

export {MyTeamComponent}