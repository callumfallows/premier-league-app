import * as React from "react";
import { MyTeamComponent } from "./MyTeam.component";

type MyTeamContainerProps = {
    onRemovePlayer: (player: any) => void
    myTeam: Array<any>
}

function MyTeamContainer(
    {
        myTeam,
        onRemovePlayer
    }: MyTeamContainerProps) {

    return <MyTeamComponent
        onRemovePlayer={onRemovePlayer}
        myTeam={myTeam}
    />

}

export {MyTeamContainer}