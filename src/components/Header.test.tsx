import * as React from "react";
import { shallow } from "enzyme";
import {Header} from "./Header";

describe("<DotAtom/>", () => {

    it("should render the atom", () => {
        const wrapper = shallow(<Header/>);
        expect(wrapper).toMatchSnapshot();
    });
});