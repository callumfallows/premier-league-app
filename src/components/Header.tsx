import * as React from 'react';

function Header({title}: { title?: string }): JSX.Element {
    return <div>Header {title}</div>
}

export {
    Header
}