import * as React from "react"
import {
    ChakraProvider
} from "@chakra-ui/react"

import {
    BrowserRouter as Router,
    Routes,
    Route,
} from "react-router-dom";
import {HomePage} from "./pages/Home.page";

import {theme} from "./theme";

export const App = () => (
    <ChakraProvider theme={theme}>
        <Router>
            <Routes>
                <Route path="/" element={<HomePage/>}/>
            </Routes>
        </Router>
    </ChakraProvider>
)
