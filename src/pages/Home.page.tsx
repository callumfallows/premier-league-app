import * as React from 'react';
import {
    useToast,
    Box,
    HStack,
    VStack,
    Text
} from "@chakra-ui/react";

import {GetLeagueData} from "../services/search";
import {PlayerSearchContainer} from "../components/player-search/PlayerSearch.container";
import {TeamSearchContainer} from "../components/team-search/TeamSearch.container";
import {MyTeamContainer} from "../components/my-team/MyTeam.container";
import {ValidateTeam} from "../utils/create-team";
import {ContainerComponent} from "../components/container/Container.component";
import {TopHeaderComponent} from "../components/molecules/TopHeader.component";

function HomePage(): JSX.Element {

    const [players, setPlayers]: any = React.useState([]);
    const [teams, setTeams]: any = React.useState([]);
    const [myTeam, setMyTeam]: any = React.useState([]);
    const [validation, setValidationMessage]: any = React.useState("");

    //get params from url
    const params: any = React.useMemo(() => {
        const urlParams: any = new URLSearchParams(window.location.search);
        const result: any = {};

        for (const [key, value] of urlParams.entries()) {
            result[key] = value;
        }

        return result;
    }, [window.location.search]);

    React.useEffect(() => {
        GetLeagueData(function (error: any, response: any) {
            if (response) {
                setPlayers(response.players)
                setTeams(response.teams)
            }
            if (error) {
                console.log("error", error, response)
                setTeams(response.teams)
            }
        })

        //If local stored team can be found
        const savedTeam = sessionStorage.getItem("currentTeam")
        if (savedTeam && savedTeam.length > 0) {
            setMyTeam(JSON.parse(savedTeam))
        }
    }, []);

    React.useEffect(() => {
        sessionStorage.setItem("currentTeam", JSON.stringify(myTeam));
    }, [myTeam])

    const toast = useToast();
    return <Box width="100%">
        <Box
            width="100%"
            backgroundColor="gray.200"
        >
            <ContainerComponent>
                <TopHeaderComponent/>
            </ContainerComponent>
        </Box>
        <ContainerComponent>
            <VStack
                margin={4}
                spacing={8}
            >
                <HStack
                    spacing={4}
                    width="100%"
                    alignItems="flex-start"
                    justifyContent="flex-start"
                >
                    <Box width="50%">
                        <PlayerSearchContainer
                            playerName={params.playerName || ""}
                            players={players}
                            onSelectPlayer={(player, position) => {

                                const {
                                    isValid,
                                    message
                                } = ValidateTeam(myTeam, player, position);

                                if (isValid) {
                                    setMyTeam([...myTeam, {player, position}]);

                                    setValidationMessage("")
                                    toast({
                                        title: 'Player added',
                                        description: "Play was added to the team.",
                                        status: 'success',
                                        duration: 9000,
                                        isClosable: true,
                                    })
                                } else {

                                    setValidationMessage(message)
                                    toast({
                                        title: 'Player not added',
                                        description: message,
                                        status: 'error',
                                        duration: 9000,
                                        isClosable: true,
                                    })
                                }
                            }}
                        />
                    </Box>
                    <Box width="50%">
                        <TeamSearchContainer
                            teamName={params.teamName || ""}
                            teams={teams}
                            onSelectTeam={(team) => console.log(team)}
                        />
                    </Box>
                </HStack>
                <Box width="100%">
                    <MyTeamContainer
                        myTeam={myTeam}
                        onRemovePlayer={(player) => {
                            setMyTeam(myTeam.filter((teamPlayer: any) => teamPlayer.player.code !== player.code))
                            setValidationMessage("");
                        }}
                    />
                </Box>
                {validation && <Box>
                    <Text color="red">
                        {validation}
                    </Text>
                </Box>}
            </VStack>
        </ContainerComponent>
    </Box>
}

export {HomePage}