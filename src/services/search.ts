import axios from "axios";
import {FantasyPremierPlayers} from "../testing/FantasyPremierPlayers.stub";
import {FantasyPremierTeamsStub} from "../testing/FantasyPremierTeamsStub.stub";

const BASE_URL = "https://fantasy.premierleague.com/api";

async function GetLeagueData(callback: (error: any, success: any) => any): Promise<any> {

    try {

        return await axios.get(`${BASE_URL}/bootstrap-static/`)
            .then(response => {
                callback(null, {
                    players: response.data.elements,
                    teams: response.data.teams
                })
                return response.data;
            });

    } catch (error: any) {
        // process.env.NODE_ENV === "development"
        if (error && error.message === "Network Error") {
            console.log("Stubbing for localhost due to CORS")

            callback(null, {
                players: FantasyPremierPlayers,
                teams: FantasyPremierTeamsStub
            })

            return {
                players: FantasyPremierPlayers,
                teams: FantasyPremierTeamsStub
            };
        }

        callback(error, null)
        return error;
    }
}

export {GetLeagueData}