import axios from "axios";

const LOGIN_URL = "https://users.premierleague.com/accounts/login/";

async function AuthenticateUser(callback: (error: any, success: any) => any): Promise<any> {

    try {

        let bodyFormData: any = new FormData();
        bodyFormData.append("login", "callum_fallows%40hotmail.co.uk")
        bodyFormData.append("password", "Cascascas3")
        bodyFormData.append("redirect_uri", "https://fantasy.premierleague.com/a/login")
        bodyFormData.append("app", "plfpl-web")

        return await axios({
                method: "post",
                url: LOGIN_URL,
                data: bodyFormData,
                headers: { "Content-Type": "text/html; charset=utf-8" },
            }
        ).then(response => {
            callback(null, response.data.elements)
            return response.data;
        });
    } catch (error: any) {
        // process.env.NODE_ENV === "development" &&
        if (error && error.message === "Network Error") {
            console.log("network error")
            callback(null, [])
            return error;
        }

        callback(error, null)
        return error;
    }

}


export {AuthenticateUser}
