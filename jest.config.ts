// jest.config.ts
import type { Config } from "@jest/types";

// Sync object
const config: Config.InitialOptions = {
    preset: "ts-jest",
    testEnvironment: "jsdom",
    "setupFilesAfterEnv": ["<rootDir>/jest.config.ts", "<rootDir>/enzyme-config.ts"],
    "snapshotSerializers": ["enzyme-to-json/serializer"],
    "modulePathIgnorePatterns": [
        "<rootDir>/public/",
        "<rootDir>/cypress/"
    ],
    "transform": {
        "^.+\\.tsx?$": "ts-jest",
        "^.+\\.svg$": "<rootDir>/svgTransform.ts"
    },
};

export default config;

