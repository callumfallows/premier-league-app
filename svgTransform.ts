// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
module.exports = {
    process() {
        return "module.exports = {};";
    },
    getCacheKey() {
        // The output is always the same.
        return "svgTransform";
    },
};
